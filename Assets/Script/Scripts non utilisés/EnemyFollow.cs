﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollow : MonoBehaviour {

    public float speed;
    public float stoppingDistance;

    private Transform target; 

    void Start(){

    // The enemy will "target" the object with the "Player" tag
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }


    void Update(){

    // Move towards the "Player" tag : if(//if he isn't close to the player then continue moving)
        if(Vector2.Distance(transform.position, target.position) > stoppingDistance){

    // The new position is a Vector 2 who take care about (from, to, speed) && Time.deltaTime : it is how much time passed since the previous frame
        transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }
    }
}

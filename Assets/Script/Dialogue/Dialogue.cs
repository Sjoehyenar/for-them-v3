﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    [System.Serializable]
public class Dialogue
{

    public string name;

    // [TextArea(min amount of line the text are will use, max amount of line the text are will use)]
    [TextArea(3, 10)] 
    public string[] sentences;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoaderAnim : MonoBehaviour

{
    public Animator transition;

    //When an object collides with the one where the Script is affected, then the "Start" animation starts
    private void OnTriggerEnter2D(Collider2D other)
    {
        transition.SetTrigger("Start");
    }

}

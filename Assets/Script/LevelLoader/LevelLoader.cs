﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class LevelLoader : MonoBehaviour
{
    
    public float transitionTime = 1f;

    //When an object collides with the one where the Script is affected, then the coroutine starts to load the next scene from the buildIndex (+1)
    private void OnTriggerEnter2D(Collider2D other)
    {
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
    }

       IEnumerator LoadLevel(int levelIndex)
    {
        //Wait (transitionTime)
        yield return new WaitForSeconds(transitionTime); 
        Debug.Log("suivant");
        //Load scene from levelIndex thanks to the Coroutine 
        SceneManager.LoadScene(levelIndex);
    }
}

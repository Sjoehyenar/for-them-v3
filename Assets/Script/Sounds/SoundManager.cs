﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static AudioClip ChienSound, ZombieSound;
    static AudioSource AudioSrc;

    // Correspondence and path of the different sounds and component activation 
    void Start()
    {
        ChienSound = Resources.Load<AudioClip>("Chien");
        ZombieSound = Resources.Load<AudioClip>("Zombie");

        AudioSrc = GetComponent<AudioSource>();
    }

    // For each case, play corresponding audio source 
    public static void PlaySound (string clip)
    {
        switch (clip)
        {
            case "Chien":
                AudioSrc.PlayOneShot(ChienSound);
                break;
            case "Zombie":
                AudioSrc.PlayOneShot(ZombieSound);
                break;
        }
    } 
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundZombie : MonoBehaviour
{
    //If there is a collision with the component with the tag "Player", then play the corresponding sound from Sound Manager
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            SoundManager.PlaySound("Zombie");
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundChien : MonoBehaviour
{
    //If there is a collision with the component with the tag "dog", then play the corresponding sound from Sound Manager
    void OnTriggerEnter2D(Collider2D soundtrig) 
    {
        if(soundtrig.transform.CompareTag("dog"))
        {
            SoundManager.PlaySound("Chien");
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public GameObject pauseMenuUI;  

    //If the Escape key is pressed, then the game is paused
    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            } else {
                Pause();
            }
        }
    }

    //When the menu is not active, the timeScale is 1f - the game is not paused
   public void Resume() {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }
    
    //When the menu is activated, the timeScale is 0f - the game is paused, the UI is activated
   void Pause() {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    //When the Menu button is clicked, the timeScale is 1f and Unity loads the Menu
    public void LoadMenu() {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");
    }

    //When the Exit button is clicked, Unity exits the game
    public void QuitGame() {
        Debug.Log("QUITTER");
        Application.Quit();
    }
}

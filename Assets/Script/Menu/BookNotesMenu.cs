﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookNotesMenu : MonoBehaviour
{
    public static bool GameIsBookNotes = false;
    public GameObject notesMenuUI;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            if (GameIsBookNotes)
            {
                Resume();
            } else 
            {
                Notes();
            }
        }
    }

    public void Resume()
    {
        notesMenuUI.SetActive(false);
        Time.timeScale = 1f; 
        GameIsBookNotes = false; 
    }

    void Notes()
    {
        notesMenuUI.SetActive(true);
        Time.timeScale = 0f; 
        GameIsBookNotes = true; 
    }
}

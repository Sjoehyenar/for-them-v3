﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class MainMenu : MonoBehaviour {
	
	// When the Play button is clicked, Unity loads the next scene from the BuildIndex (+1)
	public void PlayGame ()
	{
	SceneManager.LoadScene(SceneManager. GetActiveScene().buildIndex + 1); 
	}
	
	//When the Exit button is clicked, Unity exits the game
	public void QuitGame ()
	{
		Debug.Log("QUITTER");
		Application.Quit();
	}
}

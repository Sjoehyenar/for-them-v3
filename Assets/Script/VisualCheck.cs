﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualCheck : MonoBehaviour
{
	  public Sprite glow_red;
  	public Sprite glow_green;

  	private SpriteRenderer checkpointSpriteRenderer;
  	public bool checkpointReached;

  // Use this for initialization
  void Start () {
  	checkpointSpriteRenderer = GetComponent<SpriteRenderer> ();
  }

  void OnTriggerEnter2D(Collider2D other){
    if (other.tag == "Player") {
      checkpointSpriteRenderer.sprite = glow_green;
      checkpointReached = true;
    }
  }
}

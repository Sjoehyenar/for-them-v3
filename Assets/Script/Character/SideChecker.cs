﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideChecker : MonoBehaviour
{
    bool isColliding = false;

    void OnTriggerStay2D()
    {
        isColliding = true;
    }

    void OntriggerEnter2D()
    {
        isColliding = false; 
    }

    public bool IsColliding()
    {
        return isColliding; 
    }
}

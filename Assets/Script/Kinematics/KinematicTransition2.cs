﻿using UnityEngine;
using UnityEngine.Video;

public class KinematicTransition2 : MonoBehaviour
{
    private VideoPlayer Kinematic;

    //When the game is Awake, then get component Video Player
    void Awake()
    {
        Kinematic = GetComponent<VideoPlayer>();
        Kinematic.loopPointReached += OnMovieFinished; // loopPointReached is the event for the end of the video
    }
    //When the movie is finished, the video player stops, and Unity loads the scene Tutorial
    void OnMovieFinished(VideoPlayer player)
    {
        Debug.Log("Event for movie end called");
        player.Stop();
        Debug.Log("Movie Ended!");
        Application.LoadLevel("CreditMenu");
    }

}
